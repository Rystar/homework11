/*
оператори - це спеціальні символи або ключові
арефметичні, присвоєння, порівняльні
оператори присвоєння використовуються для порівняння двох значень і повертають булеве значення залежно від результату порівняння
 */
// наприклад

let a = 5;
let b = '5';

console.log(a == b);

let aa = 3;
let bb = 5;

console.log(aa >= bb);

/*
оператори присвоєння використовуються для присвоєння значень змінним
 */

// наприклад

let aaa = 10;
aaa -= 3;

console.log(aaa)

// 1

let userName = 'Bohdan';
let password = 'secret';
let userInput = prompt('Введіть password: ');

if (userInput === password) {
    console.log('Пароль вірний');
} else {
    console.log('Пароль не вірнимй')
}

// 2

let x = 5;
let y = 3;

let xy = x + y ;
let xy1 = x - y ;
let xy2 = x * y ;
let xy3 = x / y ;

alert(`+: ${xy}, \n-: ${xy1}, \n*: ${xy2}, \n;/: ${xy3}`)